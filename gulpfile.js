'use strict';

let gulp = require('gulp'),
plumber = require('gulp-plumber'),
browserSync = require('browser-sync'),
reload = browserSync.reload,
sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
cssMin = require('gulp-clean-css'),
preFixer = require('gulp-autoprefixer'),
gcmq = require('gulp-group-css-media-queries');

let path = {
  
  build: {
    html: 'build/',
    css: 'build/css/',
    img: 'build/images/',
  },
  src: {
    html: 'src/*.html',
    style: 'src/*.scss',
    css: 'src/scss/*.scss',
    img: 'src/images/*'
  },
  watch: {
    html: 'src/*.html',
    style: 'src/*.scss',
    css: 'src/scss/*.scss',
    img: 'src/images/'
  },
  clean: './build'
};

let config = {
  server: {
    baseDir: "./build"
  },
  tunnel: false,
  host: 'localhost',
  port: 3000,
  logPrefix: "frontend"
};

gulp.task('html:build', function () {
  return gulp.src(path.src.html)
  .pipe(gulp.dest(path.build.html))
  .pipe(reload({stream: true}));
});

gulp.task('img:build', function () {
  return gulp.src(path.src.img)
  .pipe(gulp.dest(path.build.img));
});

gulp.task('style:build', function (done) {
  gulp.src(path.src.style)
  .pipe(sourcemaps.init())
  .pipe(plumber())
  .pipe(sass())
  .pipe(preFixer({
    cascade: false
  }))
  .pipe(gcmq())
  .pipe(cssMin())
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(path.build.css))
  .pipe(reload({stream: true}));
  done()
});

gulp.task('css:build', function (done) {
  gulp.src(path.src.css)
  .pipe(sourcemaps.init())
  .pipe(plumber())
  .pipe(sass())
  .pipe(cssMin())
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(path.build.css))
  .pipe(reload({stream: true}));
  done()
});

gulp.task('build', gulp.parallel('html:build', 'style:build', 'img:build', 'css:build' ));

gulp.task('browser-sync', function () {
  browserSync(config);
});

gulp.task('watch', function () {
  gulp.watch(path.watch.style, gulp.parallel('style:build'));
  gulp.watch(path.watch.html, gulp.parallel('html:build'));
  gulp.watch(path.watch.css, gulp.parallel('css:build'));
  gulp.watch(path.watch.img, gulp.parallel('img:build'));
});

gulp.task('default', gulp.parallel('build', 'browser-sync', 'watch'));




